FROM jupyter/pyspark-notebook

COPY ./notebooks/* /notebooks/

RUN pip install -r /notebooks/requirements.txt
