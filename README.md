```
git clone https://gitlab.com/oxbits/pandas_api_demo.git

cd pandas_api_demo

docker build -t jpsnb_proj .

docker run -it --name a_jpsnb_proj --rm --publish 8420:8420 --volume $(pwd)/notebooks:/notebooks -w /notebooks jpsnb_proj bash

# you can access same container from another shell :
# docker exec -it -w /notebooks a_jpsnb_proj bash 

jupyter notebook --ip=0.0.0.0 --port=8420 --allow-root
```

open the URL from standard output in your browser : 

`http://127.0.0.1:8420/?token=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`

open the notebook :

`pandas_to_pandas_api_on_spark_in_10_minutes.ipynb`

run the notebook

in notebook UI_UX click `File` > `Close and Halt`

close browser

```
# ctrl-c then y then enter to stop jupyter notebook

exit
```

